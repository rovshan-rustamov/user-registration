package com.example.userregistration.model;
import com.example.userregistration.model.PhoneNumber;
import jakarta.persistence.*;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Entity
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @Column(unique = true)
    private String userName;
//    @Column(unique = true)
    private String email;
    private String password;
//    private List<Integer> integers;
    @OneToMany
    private List<PhoneNumber> phoneNumbers;
}
