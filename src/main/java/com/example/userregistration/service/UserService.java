package com.example.userregistration.service;

import com.example.userregistration.dto.LoginUserDto;
import com.example.userregistration.dto.ResetPasswordUserDto;
import com.example.userregistration.dto.UserDto;
import com.example.userregistration.dto.UserRegisterDto;
import com.example.userregistration.model.User;
import com.example.userregistration.repository.UserRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final EntityManagerFactory emf;
    private static Integer integer;


    public String registerUser(UserRegisterDto userRegisterDto) {
        boolean checkExistances = !userRepository.existsByEmail(userRegisterDto.getEmail()) &&
                !userRepository.existsByUserName(userRegisterDto.getUserName());
        boolean checkPasswordsMatching = userRegisterDto.getPassword().equals(userRegisterDto.getRepeatedPassword());
        if (checkExistances) {
            if (checkPasswordsMatching) {
                User user = modelMapper.map(userRegisterDto, User.class);
                userRepository.save(user);
                return "Registered";
            }
            return "Passwords do not match";
        }
        return "Email or(and) User Name already registered";
    }


    public String manualReg(UserRegisterDto userRegisterDto){
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(userRegisterDto);
        entityManager.getTransaction().commit();
        entityManager.close();
        return "registered";
    }

//    public String registerUser(UserRegisterDto userRegisterDto){
//        User user = modelMapper.map(userRegisterDto, User.class);
//        userRepository.save(user);
//        return "saved";
//    }


    public String deleteUser(Long id) {
        userRepository.deleteById(id);
        return "deleted";
    }

    public String deleteAll() {
        userRepository.deleteAll();
        return "all deleted";
    }

    public boolean loginUser(LoginUserDto loginUserDto) {
        if (userRepository.existsByUserName(loginUserDto.getUserName())) {
            User user = userRepository.findUserByUserName(loginUserDto.getUserName());
            return user.getPassword().equals(loginUserDto.getPassword());
        }
        return false;
    }

    public String resetUserPassword(ResetPasswordUserDto resetPasswordUserDto) {
        if (resetPasswordUserDto.getPassCode().equals(UserService.integer)) {
            Optional<User> userOptional = userRepository.findUserByEmail(resetPasswordUserDto.getEmail());
            boolean checkPasswords = resetPasswordUserDto.getNewPassword().
                    equals(resetPasswordUserDto.getRepeatNewPassword());
            userOptional.ifPresent(user -> {
                if (checkPasswords) {
                    user.setPassword(resetPasswordUserDto.getNewPassword());
                    userRepository.save(user);
                }


            });

        }
        return "wrong passcode";
    }

    public Integer generatePasscode(ResetPasswordUserDto resetPasswordUserDto) {
        if (userRepository.existsByEmail(resetPasswordUserDto.getEmail())) {
            Random random = new Random();
            Integer passcode = random.nextInt((999999 - 100000) + 100000) + 100000;
            UserService.integer = passcode;
            return passcode;
        }

        return 0;
    }

    public UserDto getAUser(Long id) {
        User user = userRepository.findById(id).get();
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }


    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> userDtos = new LinkedList<>();
        for (int i = 0; i < users.size(); i++) {
            userDtos.add(modelMapper.map(users.get(i), UserDto.class));
        }
        return userDtos;
    }
}
