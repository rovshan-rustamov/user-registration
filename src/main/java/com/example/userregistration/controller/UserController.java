package com.example.userregistration.controller;

import com.example.userregistration.dto.LoginUserDto;
import com.example.userregistration.dto.ResetPasswordUserDto;
import com.example.userregistration.dto.UserDto;
import com.example.userregistration.dto.UserRegisterDto;
import com.example.userregistration.service.UserService;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/user")
@Data
public class UserController {
    private final ModelMapper modelMapper;
    private final UserService userService;


//    @PostMapping
//    public String registerUser(@RequestBody UserRegisterDto userRegisterDto,
//                               @RequestParam("file")MultipartFile file) {
//        return userService.registerUser(userRegisterDto);
//    }
    @PostMapping("/manualLabor")
    public String manualLabor(@RequestBody UserRegisterDto userRegisterDto){
        return userService.manualReg(userRegisterDto);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @DeleteMapping
    public String deleteAll() {
        return userService.deleteAll();
    }

    @GetMapping
    public String loginUser(@RequestBody LoginUserDto loginUserDto) {
        return userService.loginUser(loginUserDto) ? "Logged in" : "Wrong user name or password";
    }

    @PutMapping
    public String resetUserPassword(@RequestBody ResetPasswordUserDto resetPasswordUserDto) {
        return userService.resetUserPassword(resetPasswordUserDto);
    }

    @GetMapping("/resetByEmail")
    public Integer generatePasscode(@RequestBody ResetPasswordUserDto resetPasswordUserDto) {
        return userService.generatePasscode(resetPasswordUserDto);
    }

    @GetMapping("/{id}")
    public UserDto getAUser(@PathVariable Long id) {
        return userService.getAUser(id);
    }

    @GetMapping("/findAll")
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }
}
