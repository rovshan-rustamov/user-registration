package com.example.userregistration.repository;

import com.example.userregistration.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);
    boolean existsByUserName(String userName);
    User findUserByUserName(String username);
    Optional<User> findUserByEmail(String email);
//    @Query(value = , nativeQuery = true)
//    List<User> findAllByPasswordIsLike(String pass);
}
