package com.example.userregistration.repository;

import com.example.userregistration.model.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhoneNumberRepo extends JpaRepository<PhoneNumber, Long> {
//    List<PhoneNumber> findAllByUser_Id(Long id);
}
