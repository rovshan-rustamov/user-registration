package com.example.userregistration.dto;

import lombok.Data;

@Data
public class UserRegisterDto {
    private String userName;
    private String email;
    private String password;
    private String repeatedPassword;


}
