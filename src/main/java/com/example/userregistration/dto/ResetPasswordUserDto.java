package com.example.userregistration.dto;

import lombok.Data;

@Data
public class ResetPasswordUserDto {
    private String email;
    private String password;
    private String newPassword;
    private String RepeatNewPassword;
    private Integer passCode;
}
