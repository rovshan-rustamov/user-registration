package com.example.userregistration;

import com.example.userregistration.model.PhoneNumber;
import com.example.userregistration.model.User;
import com.example.userregistration.repository.PhoneNumberRepo;
import com.example.userregistration.repository.UserRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class UserRegistrationApplication implements CommandLineRunner {
    private final PhoneNumberRepo phoneNumberRepo;
    private  final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(UserRegistrationApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        User user = userRepository.findById(2L).get();
        System.out.println(user.getPhoneNumbers());
//        JDBC
//        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","postgres");
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery("select*from users");
//        while(resultSet.next()){
//            System.out.println(resultSet.getInt(1));
//            System.out.println(resultSet.getString(2));
//            System.out.println(resultSet.getString(3));
//            System.out.println(resultSet.getString(4));

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnitName");
//        EntityManager entityManager = emf.createEntityManager();
//        entityManager.getTransaction().begin();
//        List resultList = entityManager.createNativeQuery("select * from users", User.class).
//                getResultList();
//        System.out.println(resultList);
//        entityManager.getTransaction().commit();
//        entityManager.close();
        /*
        PhoneNumber phone1 = PhoneNumber.builder()
                .number("+994554794311")
                .user(user)
                .build();
        phoneNumberRepo.save(phone1);
        PhoneNumber phone2 = PhoneNumber.builder()
                .number("+994553655574")
                .user(user)

                .build();
        phoneNumberRepo.save(phone2);

        PhoneNumber phone3 = PhoneNumber.builder()
                .number("+994555040657")
                .user(user)

                .build();
        phoneNumberRepo.save(phone3);

        user.getPhoneNumbers().add(phone1);
        user.getPhoneNumbers().add(phone2);
        user.getPhoneNumbers().add(phone3);
        userRepository.save(user);
        */
    }
}


// jdbc:postgresql://${DB_HOSTNAME:localhost}:${DB_PORT:5432}/postgres