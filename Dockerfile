FROM openjdk:17-alpine
COPY build/libs/UserRegistration-0.0.1-SNAPSHOT.jar /user-reg/
ENTRYPOINT ["java"]
CMD [ "-jar", "/user-reg/UserRegistration-0.0.1-SNAPSHOT.jar"]
